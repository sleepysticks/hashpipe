[![pipeline status](https://gitlab.com/sleepysticks/hashpipe/badges/master/pipeline.svg)](https://gitlab.com/sleepysticks/hashpipe/commits/master)
[![rather rusty](https://img.shields.io/badge/rather-rusty-%23B7410E.svg)](https://www.rustup.rs/)

# hashpipe: easy, parallel hashing of files
Difficulty of parallelizing SHA-256 for a single file: impossible.

Difficulty of parallelizing SHA-256 for all files in a directory: trivial, _except nothing does it._

`hashpipe` is a simple cli tool to help QA engineers everywhere spend less time waiting for release artifact hashes and get on with shipping the bits!

# usage
    hashpipe <sha1|sha256|sha384|sha512> <path_to_file_or_dir>
    
# output sample
    SHA256:e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855         test_files/empty.txt
    SHA256:8a39d2abd3999ab73c34db2476849cddf303ce389b35826850f9a700589b4a90         test_files/small_junk_data.bin
    SHA256:5647f05ec18958947d32874eeb788fa396a05d0bab7c1b71f112ceb7e9b31eee         test_files/large_junk_data