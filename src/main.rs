extern crate ring;
extern crate walkdir;

use std::io::Read;
use std::path::Path;

use walkdir::WalkDir;

fn print_usage(program_name: &str) {
    let program_file_name = Path::new(program_name)
        .file_name()
        .unwrap()
        .to_str()
        .unwrap();

    println!(
        "Usage: {prog} sha1|sha256|sha384|sha512 <file_or_dir>\n\
         \n\
         On success, the hash of the file(s) and filename(s) are printed, and 0 is returned.\n\
         On failure, an error message is printed, and a non-zero value is returned\n\
         \n\
         Example:\n\
         {prog} sha256 file_to_hash.txt",
        prog = program_file_name
    );
}

fn main() {
    let args: Vec<String> = std::env::args().collect();

    if args.iter().any(|arg| arg == "-h") {
        print_usage(&args[0]);
        return;
    } else if args.len() < 3 {
        print_usage(&args[0]);
        std::process::exit(1)
    }

    let digest_alg = match args[1].as_str() {
        "sha1" => &ring::digest::SHA1,
        "sha256" => &ring::digest::SHA256,
        "sha384" => &ring::digest::SHA384,
        "sha512" => &ring::digest::SHA512,
        _ => {
            print_usage(&args[0]);
            std::process::exit(1)
        }
    };
    let file_path = Path::new(&args[2]);

    if file_path.is_dir() {
        if args.len() > 3 {
            // Left in to demo sequential vs parallel speedup
            println!("mode: sequential directory");
            for f in WalkDir::new(file_path).min_depth(1) {
                let file_name = f.unwrap();

                if file_name.path().is_file() {
                    match hash_it_out(digest_alg, file_name.path()) {
                        Ok(res) => println!("{:?}\t\t{}", res, file_name.path().display()),
                        Err(_) => println!(
                            "the following file couldn't be hashed for some reason:\t{}",
                            file_name.path().display()
                        ),
                    }
                }
            }
        } else {
            println!("mode: parallel directory");
            let mut threads = vec![];

            for f in WalkDir::new(file_path).min_depth(1) {
                let file_name = f.unwrap();

                if file_name.path().is_file() {
                    threads.push(std::thread::spawn(move || {
                        match hash_it_out(digest_alg, file_name.path()) {
                            Ok(res) => println!("{:?}\t\t{}", res, file_name.path().display()),
                            Err(_) => println!(
                                "the following file couldn't be hashed for some reason:\t{}",
                                file_name.path().display()
                            ),
                        }
                    }));
                }
            }

            for t in threads {
                let _ = t.join();
            }
        }
    } else if file_path.is_file() {
        println!("mode: single file");
        match hash_it_out(digest_alg, file_path) {
            Ok(res) => println!("{:?}\t\t{}", res, file_path.display()),
            Err(_) => println!(
                "the following file couldn't be hashed for some reason:\t{}",
                file_path.display()
            ),
        }
    } else {
        println!(
            "the following path cannot be found:\t{}",
            file_path.display()
        );
        std::process::exit(1)
    }
}

fn hash_it_out(
    digest_alg: &'static ring::digest::Algorithm,
    file_path: &std::path::Path,
) -> Result<ring::digest::Digest, std::io::Error> {
    let mut ctx = ring::digest::Context::new(digest_alg);
    let mut file = std::fs::File::open(file_path)?;
    let mut chunk = vec![0u8; 128 * 1024];

    loop {
        let bytes_read = file.read(&mut chunk[..])?;
        if bytes_read == 0 {
            break;
        }
        ctx.update(&chunk[0..bytes_read]);
    }

    Ok(ctx.finish())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_hash_it_out_sha1() {
        assert_eq!(
            &ring::test::from_hex("da39a3ee5e6b4b0d3255bfef95601890afd80709").unwrap()[..],
            hash_it_out(&ring::digest::SHA1, Path::new("./test_files/empty.txt"))
                .unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex("2e000fa7e85759c7f4c254d4d9c33ef481e459a7").unwrap()[..],
            hash_it_out(
                &ring::digest::SHA1,
                Path::new("./test_files/small_junk_data.bin"),
            ).unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex("7d76d48d64d7ac5411d714a4bb83f37e3e5b8df6").unwrap()[..],
            hash_it_out(
                &ring::digest::SHA1,
                Path::new("./test_files/large_junk_data"),
            ).unwrap()
                .as_ref()
        );
    }

    #[test]
    fn test_hash_it_out_sha256() {
        assert_eq!(
            &ring::test::from_hex(
                "e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855",
            ).unwrap()[..],
            hash_it_out(&ring::digest::SHA256, Path::new("./test_files/empty.txt"))
                .unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "8a39d2abd3999ab73c34db2476849cddf303ce389b35826850f9a700589b4a90",
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA256,
                Path::new("./test_files/small_junk_data.bin"),
            ).unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "5647f05ec18958947d32874eeb788fa396a05d0bab7c1b71f112ceb7e9b31eee",
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA256,
                Path::new("./test_files/large_junk_data"),
            ).unwrap()
                .as_ref()
        );
    }

    #[test]
    fn test_hash_it_out_sha384() {
        assert_eq!(
            &ring::test::from_hex(
                "38b060a751ac96384cd9327eb1b1e36a21fdb71114be07434c0cc7bf63f6e1da274edebfe76f65fbd\
                 51ad2f14898b95b"
            ).unwrap()[..],
            hash_it_out(&ring::digest::SHA384, Path::new("./test_files/empty.txt"),)
                .unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "9bf066f52cd8588b2131d1d2da24b2adacb8ad3efc36f3cb779772c193a0b140b120bd13dfcb3e5e8\
                 c66b68526d53150"
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA384,
                Path::new("./test_files/small_junk_data.bin"),
            ).unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "6f71dee19ba3fbdc5c15e857c98727eb91c318321c1c8d5a716a6b5d1b0404acb2a62fd9755625457\
                 01013ec7f99329f"
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA384,
                Path::new("./test_files/large_junk_data"),
            ).unwrap()
                .as_ref()
        );
    }

    #[test]
    fn test_hash_it_out_sha512() {
        assert_eq!(
            &ring::test::from_hex(
                "cf83e1357eefb8bdf1542850d66d8007d620e4050b5715dc83f4a921d36ce9ce47d0d13c5d85f2b0f\
                 f8318d2877eec2f63b931bd47417a81a538327af927da3e",
            ).unwrap()[..],
            hash_it_out(&ring::digest::SHA512, Path::new("./test_files/empty.txt"),)
                .unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "9dd0c30167fbeaf68dfbbad8e1af552a7a1fcae120b6e04f1b41fa76c76d5a78922ff828f5cffd8c0\
                 2965cde57d63dcbfb4c479b3cb49c9d8107a7d5244e9d03",
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA512,
                Path::new("./test_files/small_junk_data.bin"),
            ).unwrap()
                .as_ref()
        );

        assert_eq!(
            &ring::test::from_hex(
                "731859029215873fdac1c9f2f8bd25a334abf0f3a9e1b057cf2cacc2826d86b0c26a3fa920a936421\
                 401c0471f38857cb53ba905489ea46b185209fdff65b3b6",
            ).unwrap()[..],
            hash_it_out(
                &ring::digest::SHA512,
                Path::new("./test_files/large_junk_data"),
            ).unwrap()
                .as_ref()
        );
    }
}
